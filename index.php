<?php

/**
 * A real application will have a more complex configuration system.
 * In our case, this simple method is suitable.
 */
$configuration = [
    'RestApi' => [
        'register' => [
            'method' => 'POST',
            'url' => 'https://api.supermetrics.com/assignment/register',
            'clientId' => 'ju16a6m81mhid5ue1z3v2g0uh',
            'clientEmail' => 'jb@stoiss.net',
            'clientName' => 'Stoiss'
        ],
        'posts' => [
            'method' => 'GET',
            'url' => 'https://api.supermetrics.com/assignment/posts'
        ]
    ]
];

spl_autoload_register(function ($className) {
    require str_replace(['Supermetrics', '\\'], ['src', '/'] , $className) . '.php';
});

if (($_GET['type'] ?? '') == 'csv') {
    (new \Supermetrics\Apps\Csv($configuration))->run();
} else {
    (new \Supermetrics\Apps\Json($configuration))->run();
}