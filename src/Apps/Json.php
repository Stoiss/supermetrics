<?php

namespace Supermetrics\Apps;


use Supermetrics\Aggregators\AverageCharsMonth;
use Supermetrics\Aggregators\AverageUserPostsMonth;
use Supermetrics\Aggregators\LongestPostMonth;
use Supermetrics\Aggregators\PostsWeek;
use Supermetrics\DataSources\IDataSource;
use Supermetrics\DataSources\RestApi;
use Supermetrics\Reports\IReport;


/**
 * Application returns report in JSON format
 *
 * Class Json
 */
class Json extends App
{
    protected $configuration;

    public function __construct(array $configuration = [])
    {
        $this->configuration = $configuration;
    }

    protected function createDataSource(): IDataSource
    {
        return new RestApi($this->configuration['RestApi']);
    }

    protected function createAggregators(): array
    {
        return [
            'AverageCharactersByMonth' => new AverageCharsMonth(),
            'LongestPostByMonth' => new LongestPostMonth(),
            'PostsByWeek' => new PostsWeek(),
            'AverageUserPostsMonth' => new AverageUserPostsMonth()
        ];
    }

    protected function createReport(): IReport
    {
        return new \Supermetrics\Reports\Json();
    }
}