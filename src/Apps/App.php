<?php

namespace Supermetrics\Apps;


use Supermetrics\Aggregators\IAggregator;
use Supermetrics\DataSources\IDataSource;
use Supermetrics\Reports\IReport;

/**
 * Abstract factory.
 * It helps to add new ways to obtain, aggregate and display data.
 *
 * In this example, I made two ways of displaying data: JSON and CSV files
 * in order to show how to maintenance the application.
 *
 * Class App
 * @package Supermetrics\Apps
 */
abstract class App
{
    /**
     * Abstraction method for instantiating an IDataSource interface
     *
     * @return IDataSource
     */
    abstract protected function createDataSource(): IDataSource;

    /**
     * Abstraction method returns an array of instances of IAggregator interfaces
     *
     * @return array
     */
    abstract protected function createAggregators(): array;

    /**
     * Abstraction method for instantiating an IReport interface
     *
     * @return IReport
     */
    abstract protected function createReport(): IReport;

    /**
     * The main login of the application
     */
    public function run()
    {
        $dataSource = $this->createDataSource();
        $aggregators = $this->createAggregators();
        $report = $this->createReport();

        /**
         * @var $aggregator IAggregator
         */
        while ($post = $dataSource->fetch()) {
            foreach ($aggregators as $aggregator) {
                $aggregator->addPost($post);
            }
        }

        foreach ($aggregators as $name => $aggregator) {
            $report->addValue(
                $name,
                $aggregator->makeReport($this->createReport())
            );
        }

        $report->make();
    }
}