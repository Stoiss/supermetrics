<?php

namespace Supermetrics\DataSources;


/**
 * The exception helps to catch and work with errors
 * in case of application growth
 *
 * Class DataSourceException
 */
class DataSourceException extends \Exception
{
    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
    }

}