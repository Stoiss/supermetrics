<?php

namespace Supermetrics\DataSources;


use Supermetrics\Models\Post;

/**
 * Interface users for link any data source of posts
 * with the application
 *
 * Interface IDataSource
 */
interface IDataSource 
{
    /**
     * Returns a next post, should be used in cycle
     * to get rows one by one. When all rows have been got,
     * must return null
     *
     * @return Post|null
     */
    public function fetch(): ?Post;

    /**
     * When nothing more to return - true, otherwise - false
     *
     * @return bool
     */
    public function eof(): bool;
}