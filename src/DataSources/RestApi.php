<?php

namespace Supermetrics\DataSources;


use Supermetrics\Models\IPost;
use Supermetrics\Models\Post;

/**
 * An implementation of IDataSource interface
 * which works with Supermetrics API
 *
 * Class RestApi
 */
class RestApi implements IDataSource
{
    protected $configuration;

    /**
     * Current token
     *
     * @var
     */
    protected $token;

    /**
     * A next page which will be requested from API
     *
     * @var int
     */
    protected $page = 1;

    /**
     * Posts to work, requested from API
     *
     * @var array
     */
    protected $posts = [];

    /**
     * Cursor for $posts array
     *
     * @var int
     */
    protected $counter = 0;

    /**
     * When nothing more to return - true, otherwise - false
     *
     * @var bool
     */
    protected $eof = false;

    /**
     * RestApi constructor.
     *
     * @param array $configuration  params to initialize an instance,
     *                              array must consist 'register' and 'posts' elements
     */
    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Makes a REST request
     *
     * In more complex application it would be better to move this logic
     * to its own class in order to segregate responsibility
     *
     * @param string $method  HTTP method, GET or POST
     * @param string $url  url to request
     * @param array|null $params  params for request
     * @return array  data of response
     * @throws DataSourceException
     */
    protected function apiRequest(string $method, string $url, array $params = null): array
    {
        $curl = curl_init();

        switch ($method)
        {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($params) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
                }
                break;
            default:
                if ($params) {
                    $url = sprintf('%s?%s', $url, http_build_query($params));
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (empty($result)) {
            throw new DataSourceException('API Connection failed');
        }

        $data = json_decode($result, true);

        if (empty($data) || !is_array($data)) {
            throw new DataSourceException('Received data is empty or incorrect');
        }

        if (isset($data['error'])) {
            throw new DataSourceException($data['error']['message'] ?? 'Incorrect request');
        }

        curl_close($curl);

        return $data;
    }

    /**
     * Gets a token from the Supermetrics API
     *
     * @return string  token
     * @throws DataSourceException
     */
    protected function requestToken(): string
    {
        $config = $this->configuration['register'];

        $result = $this->apiRequest(
            $config['method'],
            $config['url'],
            [
                'client_id' => $config['clientId'],
                'email' => $config['clientEmail'],
                'name' => $config['clientName']
            ]
        );

        if (!isset($result['data']['sl_token'])) {
            throw new DataSourceException('Request token process has failed');
        }

        return $result['data']['sl_token'];
    }

    /**
     * Gets posts
     *
     * @param string $token  token for access to API
     * @param int $page  number of a requested page
     * @return array|null
     * @throws DataSourceException
     */
    protected function requestPosts(string $token, int $page = 1): ?array
    {
        $config = $this->configuration['posts'];

        $result = $this->apiRequest(
            $config['method'],
            $config['url'],
            [
                'sl_token' => $token,
                'page' => $page
            ]
        );

        if (!isset($result['data']['posts']) || !isset($result['data']['page'])) {
            throw new DataSourceException('Request posts process has failed');
        }

        if ($page > $result['data']['page']) {
            return null;
        }

        return $result['data']['posts'];
    }

    /**
     * Converts a Supermetrics view of the post to a 'Post' object
     *
     * @param array $data
     * @return Post
     * @throws DataSourceException
     */
    protected function convertToModel(array $data): Post
    {
        if (empty($data['id']) || empty($data['from_id']) || empty($data['created_time'])) {
            throw new DataSourceException('Post structure is invalid');
        }

        $post = new Post();
        $post->setId($data['id']);
        $post->setUserId($data['from_id']);
        $post->setUserName($data['from_name'] ?? '');
        $post->setMessage($data['message'] ?? '');
        $post->setType($data['type']);
        $post->setCreatedTime(new \DateTime($data['created_time']));

        return $post;
    }

    /**
     * Returns one post.
     * An implementation of the IDataSource interface
     *
     * @return Post|null
     * @throws DataSourceException
     */
    public function fetch(): ?Post
    {
        if ($this->eof) {
            return null;
        }

        if ($this->counter < count($this->posts)) {
            return $this->convertToModel($this->posts[$this->counter++]);
        }

        $this->token = $this->token ?: $this->requestToken();

        $this->posts = $this->requestPosts($this->token, $this->page);

        if (empty($this->posts)) {
            $this->eof = true;
            return null;
        }

        $this->page++;
        $this->counter = 0;

        return $this->fetch();
    }

    /**
     * Returns true in case of there are no more posts to return
     *
     * @return bool
     */
    public function eof(): bool
    {
        return $this->eof;
    }
}