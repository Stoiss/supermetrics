<?php

namespace Supermetrics\Reports;


/**
 * Generates csv files for each aggregation and packs them to a zip archive
 *
 * Class Csv
 */
class Csv implements IReport
{
    /**
     * Data for report, added by addValue() function
     *
     * @var array
     */
    protected $data = [];

    /**
     * Temporary file where this report is saved
     * CSV or ZIP
     *
     * @var string
     */
    protected $filename;

    /**
     * The OS usually deletes temporary files on its own,
     * but it would be better if the application does this too.
     *
     */
    public function __destruct()
    {
        $filename = $this->getFileName();

        if ($filename && file_exists($filename)) {
            unlink($filename);
        }
    }

    public function getFileName(): string
    {
        return $this->filename;
    }

    public function addValue(string $period, $value): void
    {
        $this->data[$period] = $value;
    }

    /**
     * Saves data to a CSV file and packs CSV files into a ZIP file.
     * If there is no more data, sends the zip file to a browser.
     */
    public function make(): void
    {
        $files = [];

        $this->filename = tempnam(sys_get_temp_dir(), 'csv');
        $fp = fopen($this->filename, 'w');

        foreach ($this->data as $key => $value) {
            if ($value instanceof self) {
                $value->make();
                $files[$key] = $value->getFileName();
            } else if (is_array($value)) {
                array_walk($value, function(&$v, $k) {
                    $v = $k . '=' . $v;
                });
                fputcsv($fp, [$key, implode($value, ' ')], ';');
            } else if (is_scalar($value)) {
                fputcsv($fp, [$key, $value], ';');
            }
        }

        fclose($fp);

        $fileSize = filesize($this->filename);

        if ($fileSize) {
            $files[] = $this->filename;
        }

        if (count($files) > 1 || (count($files) == 1 && !$fileSize)) {
            $this->packFiles($files);
        }

        if (count($files) && !$fileSize) {
            $this->downloadFile();
        }
    }

    private function packFiles(array $files): void
    {
        $this->filename = tempnam(sys_get_temp_dir(), 'csv');

        $zip = new \ZipArchive;
        $res = $zip->open($this->filename, \ZipArchive::CREATE);

        if ($res === TRUE) {
            foreach ($files as $key => $file) {
                $zip->addFile($file, $key . '.csv');
            }
            $zip->close();
        }
    }

    private function downloadFile(): void
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="Report.zip"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->filename));
        readfile($this->filename);
    }
}