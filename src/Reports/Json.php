<?php

namespace Supermetrics\Reports;


/**
 * Shows the report like JSON data
 *
 * Class Json
 */
class Json extends \ArrayIterator implements \Iterator, IReport
{
    public function addValue(string $period, $value): void
    {
        $this[$period] = $value;
    }

    public function make(): void
    {
        header('Content-Type: application/json');
        echo json_encode($this);
    }
}