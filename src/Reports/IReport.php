<?php

namespace Supermetrics\Reports;


/**
 * Interface for generate simple key-value reports
 *
 * Interface IReport
 */
interface IReport
{
    /**
     * Adds a value to the report
     *
     * @param string $period
     * @param mixed $value
     */
    public function addValue(string $period, $value): void;

    /**
     * Generates the full report
     */
    public function make(): void;
}