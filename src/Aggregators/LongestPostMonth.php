<?php

namespace Supermetrics\Aggregators;


use Supermetrics\Models\Post;
use Supermetrics\Reports\IReport;

/**
 * Calculates longest post by character length / month
 *
 * Class LongestPostMonth
 */
class LongestPostMonth implements IAggregator
{
    protected $data = [];

    public function addPost(Post $post): void
    {
        $month = $post->getCreatedTime()->format('Ym');
        $key = $month . '01-' . $month . $post->getCreatedTime()->format('t');

        if (!isset($this->data[$key])) {
            $this->data[$key] = [
                'length' => $post->getMessageLength(),
                'id' => $post->getId()
            ];

            return;
        }

        if ($post->getMessageLength() > $this->data[$key]['length']) {
            $this->data[$key] = [
                'length' => $post->getMessageLength(),
                'id' => $post->getId()
            ];
        }
    }

    public function makeReport(IReport $report): IReport
    {
        foreach ($this->data as $key => $item) {
            $report->addValue($key, $item);
        }

        return $report;
    }
}