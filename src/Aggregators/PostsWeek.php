<?php

namespace Supermetrics\Aggregators;


use Supermetrics\Models\Post;
use Supermetrics\Reports\IReport;

/**
 * Calculates total posts split by week
 *
 * Class PostsWeek
 */
class PostsWeek implements IAggregator
{
    protected $data = [];

    public function addPost(Post $post): void
    {
        $date = $post->getCreatedTime();
        $monday = $date->sub(new \DateInterval('P' . ($post->getCreatedTime()->format('N') - 1) . 'D'))->format('Ymd');
        $sunday = $date->add(new \DateInterval('P6D'))->format('Ymd');
        $key = $monday . '-' . $sunday;

        if (!isset($this->data[$key])) {
            $this->data[$key] = 0;
        }

        $this->data[$key]++;
    }

    public function makeReport(IReport $report): IReport
    {
        foreach ($this->data as $key => $item) {
            $report->addValue($key, $item);
        }

        return $report;
    }
}