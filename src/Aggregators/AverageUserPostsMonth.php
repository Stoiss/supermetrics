<?php

namespace Supermetrics\Aggregators;


use Supermetrics\Models\Post;
use Supermetrics\Reports\IReport;

/**
 * Calculates the average number of posts per user / month
 *
 * Class AverageUserPostsMonth
 */
class AverageUserPostsMonth implements IAggregator
{
    protected $data = [];

    public function addPost(Post $post): void
    {
        $month = $post->getCreatedTime()->format('Ym');
        $key = $month . '01-' . $month . $post->getCreatedTime()->format('t');

        if (!isset($this->data[$key])) {
            $this->data[$key] = [];
        }

        if (!isset($this->data[$key][$post->getUserId()])) {
            $this->data[$key][$post->getUserId()] = 1;
            return;
        }

        $this->data[$key][$post->getUserId()]++;
    }

    public function makeReport(IReport $report): IReport
    {
        foreach ($this->data as $key => $item) {
            $totalPosts = 0;
            foreach ($item as $userPosts) {
                $totalPosts += $userPosts;
            }

            $report->addValue($key, round($totalPosts / max(count($item), 1), 2));
        }

        return $report;
    }
}