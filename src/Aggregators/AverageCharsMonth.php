<?php

namespace Supermetrics\Aggregators;


use Supermetrics\Models\Post;
use Supermetrics\Reports\IReport;

/**
 * Calculates the average character length of a post / month
 *
 * Class AverageCharsMonth
 */
class AverageCharsMonth implements IAggregator
{
    protected $data = [];

    public function addPost(Post $post): void
    {
        $month = $post->getCreatedTime()->format('Ym');
        $key = $month . '01-' . $month . $post->getCreatedTime()->format('t');

        if (!isset($this->data[$key])) {
            $this->data[$key] = [
                'chars' => 0,
                'posts' => 0
            ];
        }

        $this->data[$key]['chars'] += $post->getMessageLength();
        $this->data[$key]['posts']++;
    }

    public function makeReport(IReport $report): IReport
    {
        foreach ($this->data as $key => $item) {
            $report->addValue($key, round($item['chars'] / max($item['posts'], 1), 2));
        }

        return $report;
    }
}