<?php

namespace Supermetrics\Aggregators;


use Supermetrics\Models\Post;
use Supermetrics\Reports\IReport;

/**
 * Aggregators collect posts to make some aggregate calculations
 *
 * Interface IAggregator
 */
interface IAggregator
{
    /**
     * Adds a post into an aggregator
     *
     * @param Post $post
     */
    public function addPost(Post $post): void;

    /**
     * Makes calculations and return them as an instance of an IReport interface
     *
     * @param IReport $report  an instance of the IReport interface
     * @return IReport  an instance with calculated data
     */
    public function makeReport(IReport $report): IReport;
}