<?php

namespace Supermetrics\Models;


/**
 * The model reflects one post
 *
 * Class Post
 */
class Post
{
    /**
     * Post ID
     *
     * @var string
     */
    protected $id;

    /**
     * User ID who posted it
     *
     * @var string
     */
    protected $userId;

    /**
     * User name who posted it
     *
     * @var string
     */
    protected $userName;

    /**
     * Message
     *
     * @var string
     */
    protected $message;

    /**
     * Type
     *
     * @var string
     */
    protected $type;

    /**
     * Date of created
     *
     * @var \DateTime
     */
    protected $createdTime;

    /**
     * Message length. Calculated automatically.
     *
     * @var int
     */
    protected $messageLength;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;
        $this->messageLength = strlen($message);
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getCreatedTime(): \DateTime
    {
        return $this->createdTime;
    }

    public function setCreatedTime(\DateTime $createdTime): self
    {
        $this->createdTime = $createdTime;
        return $this;
    }

    public function getMessageLength(): int
    {
        return $this->messageLength;
    }
}